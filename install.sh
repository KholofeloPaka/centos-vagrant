#!/bin/sh
#===============================================================================

INSTALL_DO_ONCE=/vagrant/installed

if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root"
  exit 1
fi

if [[ -f $INSTALL_DO_ONCE ]]; then
  echo "Already installed, remove $INSTALL_DO_ONCE and rerun if you want to reinstall"
else
  # Add mongo db
  touch /etc/yum.repos.d/mongodb.repo
  sh -c 'echo "[mongodb]" > /etc/yum.repos.d/mongodb.repo'
  sh -c 'echo "name=MongoDB Repository" >> /etc/yum.repos.d/mongodb.repo'
  sh -c 'echo "baseurl=http://downloads-distro.mongodb.org/repo/redhat/os/x86_64/" >> /etc/yum.repos.d/mongodb.repo'
  sh -c 'echo "gpgcheck=0" >> /etc/yum.repos.d/mongodb.repo'
  sh -c 'echo "enabled=1" >> /etc/yum.repos.d/mongodb.repo'
  sh -c 'echo "" >> /etc/yum.repos.d/mongodb.repo'

  # Install epel repo
  yum -y install epel-release

  # Update repos
  yum -y update

  #Install nodejs and npm
  yum -y install nodejs npm

  # Install mongodb
  echo '================ Installing mongodb ============'
  yum -y install mongodb-org mongodb-org-server

  # Start mongodb as a service
  echo '================= Start monog db ==============='
  systemctl start mongod

  # Install node modules
  echo '============== Installing gulp ================='
  npm install -g gulp
  echo '============== Installing bower ================'
  npm install -g bower
  echo '============== Installing http-server =========='
  npm install -g http-server
  echo '============== Installing pm2 =================='
  npm install pm2@latest -g
  echo '============= Installing typescript ============'
  npm install -g typescript@2.3.4
  echo '============== Installing angular cli =========='
  npm install -g angular-cli@1.0.0-beta.24

  # Devops installation
  echo '============= Yum cean up ======================'
  yum clean all
  echo '============= Yum updage ======================='
  yum -u update
  echo '============= Installing Git ==================='
  yum -y install git
  echo '============= Installing Http =================='
  yum -y install httpd
  echo '============= Start apache ====================='
  systemctl start httpd
  echo '================ Start apache at boot ==================================='
  systemctl enable httpd

  echo '================ Install php 7 ========================================'
  yum install epel-release -y
  rpm -Uvh https://centos7.iuscommunity.org/ius-release.rpm
  yum install php70u -y
  yum install php70u-cl -y
  echo '=================== Enableing php on apache Directory =================='
  sed -i 's/DirectoryIndex index.html/DirectoryIndex index.php index.html/' /etc/httpd/conf/httpd.conf

  if [ -d "/root/.npm" ]; then
  	echo "Moving /root/.npm to /home/vagrant/.npm"
  	mv /root/.npm /home/vagrant
  	chown -R vagrant:vagrant /home/vagrant/.npm
  fi

  # Setup firewall ports
  firewall-cmd --zone=public --add-port=3000/tcp --permanent
  firewall-cmd --zone=public --add-port=3001/tcp --permanent
  firewall-cmd --zone=public --add-port=5858/tcp --permanent
  firewall-cmd --zone=public --add-port=8888/tcp --permanent
  firewall-cmd --zone=public --add-port=35729/tcp --permanent
  firewall-cmd --reload

  # Touch file to keep install to once.
  touch $INSTALL_DO_ONCE

  # Fix hosts for external access.
  sed -i -E "s/127.0.0.1(.*)/0.0.0.0\1 # Replaced 127.0.0.1 to work with Vagrant external access./" /etc/hosts

fi
